// Відповіді на питання:

// 1.	Які існують типи даних в JavaScript?
// В JavaScript існують наступні типи даних:
// •	Number – для чисел;
// •	String – для рядків;
// •	BigInt – для дуже великих чисел, використовується в криптографії;
// •	Boolean – логічний оператор (правда/брехня);
// •	Null – для невідомих значень;
// •	Undefined – для не знайдених значень;
// •	Object – для складних структур;
// •	Symbol – для унікальних ідентифікаторів;

// 2.	В чому різниця між == і ===?
// Це два оператори порівняння. 
// Різниця між ними полягає в тому що оператор === порівнює не тільки значення операндів, а й типи. Він має назву оператор суворої рівності. А оператор == порівнює тільки значення, не звертаючи увагу на тип операндів. Його називають оператором не строгої рівності.


// 3.	Що таке оператор?
// Оператор в JS - це  те за допомогою чого можна виконувати дії з операндами.Їх можна названи внутрішніми функціями які вже вбудовані в JavaScript. Оператори бувають математичні ( +, -, /, *,%), логічні ( ||, &&, !), оператори присвоювання(=), оператори порівняння (==, ===, >, <, >=, <=, !=,!==).

// Завдання

let userName = prompt("What's Your Name?", "").trim();
while (!userName || !isNaN(userName)) {
    alert("Sorry you didn't enter a name");
    userName = prompt("What's Your Name?", `${userName}`).trim();
}

let age = prompt("How old are you", "").trim();

while (
    isNaN(age) ||
    age === undefined ||
    age === "" ||
    age === null) {
    alert("Sorry, you didn't enter your age");
    age = prompt("How old are you", `${age}`).trim();
}

if (age < 18) {
    alert('You are not allowed to visit this website');
} else if (18 >= age || age <= 22) {
    let answer = confirm("Are you sure you want to continue?");
    if (answer === true) {
        alert(`Welcome, ${userName}`);
    }
    if (answer === false) {
        alert('You are not allowed to visit this website');
    }
} else {
    alert(`Welcome, ${userName}`);
}


